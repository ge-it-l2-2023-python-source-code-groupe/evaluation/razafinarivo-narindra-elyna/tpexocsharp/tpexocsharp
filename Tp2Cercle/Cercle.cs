using System;
using tppoint;
namespace tpcercle;
public partial class Cercle{
     private double pi = Math.PI;

    public Point Point1 {get;set;}
    
    public double Rayon {get;set;}

    public Cercle(Point point,double rayon){
        Point1 = point;
        Rayon = rayon;
    }
     public double GetPerimeter(){
        return pi *2*Rayon; 
    }

    public double GetSurface(){
        return pi*Rayon*Rayon;
    }

    public void IsInclude(Point p){
        double distance = Math.Sqrt((p.X - Point1.X) * (p.X - Point1.X) + (p.Y - Point1.Y) * (p.Y - Point1.Y));
        if(distance <= Rayon){
            Console.WriteLine("Le point appartient au cercle.");
        } else{
            Console.WriteLine("Le point n'appartient pas au cercle.");
        }
    }

    public void Display(){
        Console.WriteLine($"CERCLE({Point1.X},{Point1.Y},{Rayon})");
    }
}