using System;
namespace tppoint;
public partial class Point{
    public double X{get;set;}
    public double Y{get;set;}
   
    public Point(double x,double y){
        X = x;
        Y = y;
    }
    public void Display(){
        Console.WriteLine($"POINT({X},{Y})");
    }
}