﻿using System;
using tppoint;
using tpcercle;
namespace cercle;
static class ProgramCercle
{
    public static void MainCercle()
    {
        Console.Write("Donner l'abscisse du centre:");
        bool a = double.TryParse(Console.ReadLine()??"1", out double abs);
        Console.Write("Donner l'ordonnée du centre:");
        a = double.TryParse(Console.ReadLine()??"1", out double ord);
        Console.Write("Donner le rayon:");
        a = double.TryParse(Console.ReadLine()??"1", out double rayon);
        Console.WriteLine("");
        Point Point1 = new Point(abs,ord);
        Cercle monCercle = new Cercle(Point1,rayon);
        monCercle.Display();
        Console.WriteLine($"Le périmètre est : {monCercle.GetPerimeter():F2}");
        Console.WriteLine($"La surface est : {monCercle.GetSurface():F2}");

        Console.WriteLine("");
        Console.WriteLine("Donner un point;");
        Console.Write("X : ");
        a = double.TryParse(Console.ReadLine()??"1", out double x);
        Console.Write("Y : ");
        a = double.TryParse(Console.ReadLine()??"1", out double y);
        Console.WriteLine("");
        Point Point2 = new Point(x,y);
        Point2.Display();
        monCercle.IsInclude(Point2);
    }
}
