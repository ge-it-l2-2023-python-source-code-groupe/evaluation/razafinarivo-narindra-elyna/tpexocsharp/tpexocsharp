using System;
namespace GestionJoueurs;
static partial class Joueurs {
    public static void AjouterJoueur(String nom) {
        liste += nom + ",0;";
    }
    public static String GetNomJoueur(Int32 index) {
        return liste.Split(";")[index].Split(",")[0];
    }
    public static String GetScoreJoueur(Int32 index) {
        return liste.Split(";")[index].Split(",")[1];
    }
    public static void AjouterScoreJoueur(String nom, Int32 score) {
        String tmpListeJoueurs = " ";
        for(Int32 i = 0; i < joueurs; i++) {
            if(GetNomJoueur(i).Equals(nom)) {
                score += Int32.Parse(GetScoreJoueur(i));
                tmpListeJoueurs += GetNomJoueur(i) + "," + score + ";";
            } else {
                tmpListeJoueurs += GetNomJoueur(i) + "," + GetScoreJoueur(i) + ";";
            }
        }
        liste = tmpListeJoueurs;
    }
    public static void SetScoresJoueurs(String listePoints) {
        for(Int32 i = 0; i < joueurs; i++) {
            AjouterScoreJoueur(GetNomJoueur(i), Int32.Parse(listePoints.Split(",")[i]));
        }
    }
    public static String FormaterScore() {
        String affichageScore = "";
        for(Int32 i = 0; i < joueurs; i++) {
                affichageScore += "- " + GetNomJoueur(i) + " : " + GetScoreJoueur(i) + " points\n";
        }
        return affichageScore;
    }
    public static Int32 Jouer() {
        Random rnd = new Random();
        Int32 val = rnd.Next(1, 7);

        if(val == 1) {
            Console.WriteLine(un);
        } else if(val == 2) {
            Console.WriteLine(deux);
        } else if(val == 3) {
            Console.WriteLine(trois);
        } else if(val == 4) {
            Console.WriteLine(quatre);
        } else if(val == 5) {
            Console.WriteLine(cinq);
        } else {
            Console.WriteLine(six);
        }

        return val;
    }
}