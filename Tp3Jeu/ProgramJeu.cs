﻿using System;
using System.Net.NetworkInformation;
using GestionJoueurs;
using MancheJeu;
namespace jeu;

public class ProgramJeu
{
    public static void MainJeu()
    {
        String listeManches = "";
        Joueurs.liste = "";
        Joueurs.points = "";
        Joueurs.joueurs = 0;

        Console.Write("Entrez le nombre de joueurs : ");
        Int32 nb;
        Boolean a = Int32.TryParse(Console.ReadLine(), out nb);

        while(a == false)
        {
            Console.Write("Entrez le nombre de joueurs : ");
            a = Int32.TryParse(Console.ReadLine(), out nb);
        }
        Joueurs.joueurs = nb;

        Int32 num = 1;
        while(true) {
            Console.Write($"Entrez le nom du joueur n° {num} : ");
            String nom = Console.ReadLine() ?? "";

            Joueurs.AjouterJoueur(nom);

            if(num == Joueurs.joueurs) {
                break;
            }
            num++;
        }

        while(true) {
            Console.WriteLine("\nScores actuels : ");
            Console.Write(Joueurs.FormaterScore());
            Console.ReadKey();
            Console.Write($@"
Choisissez une option : 
1 - Nouvelle manche 
2 - Voir l'historique des manches 
3 - Quitter 

Quel est votre choix ? ");

            String choix = Console.ReadLine() ?? "";

            if(choix.Equals("1")) {
                Manche thisManche = new Manche(Joueurs.joueurs, Joueurs.liste);
                thisManche.DateHeure = DateTime.Now.ToString();

                num = 1;
                String resultat = "";
                while(true) {
                    while(true) {
                        Console.Write($"\nTour de {Joueurs.GetNomJoueur(num-1)} -- Voulez-vous lancer le dé ? (y/n) ");
                        choix = Console.ReadLine() ?? "";

                        if(choix.Equals("y")) {
                            resultat += Joueurs.Jouer() + ",";
                            break;
                        } else if(choix.Equals("n")) {
                            resultat += 0 + ",";
                            Console.WriteLine("Vous avez passer votre tour.");
                            break;
                        }else{
                            Console.WriteLine("Saisie incorrect. Tapez sur 'y' pour oui et 'n' pour non. ");
                        } 

                    }

                    if(num == Joueurs.joueurs) {
                        break;
                    }
                    num++;
                }

                thisManche.SetPointsJoueurs(resultat);
                Joueurs.SetScoresJoueurs(thisManche.PointsJoueurs);
                thisManche.EtatScores = Joueurs.FormaterScore();

                listeManches += thisManche.InfoManche() + "\n";
            } else if(choix.Equals("2")) {
                Console.WriteLine(listeManches);
            } else if(choix.Equals("3")) {
                break;
            }
        }
    }
}
