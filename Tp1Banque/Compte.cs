using System;
using clientBanque;
namespace compteBanque;

class Compte{
    private float solde;
    public float Solde { get { return solde; } }
    private int code;
    public int Code { get { return code; } }
    public Client Proprietaire { get; set; }

    private static int compteur = 0;
    public void Afficher()
    {
        Console.WriteLine($"Numero de compte : {Code}");
        Console.WriteLine($"Solde du compte : {Solde}"); 
        Console.WriteLine("Propriétaire du compte :");
        Proprietaire.Afficher();
    }

    public static void ReinitialiserCompteur() {
        compteur = 0;
    }

    public static string NombreCompteCree(){
        return $"\n\nLe nombre de comptes crées: {compteur}";
    }
    public void Debiter(float montant, Compte compteClient){
        Debiter(montant);
        compteClient.Crediter(montant);
    }

    public void Debiter(float montant)
    {
        solde -= montant;
    }

    public void Crediter(float montant, Compte compteClient)
    {
        Crediter(montant);
        compteClient.Debiter(montant);
    }
    public void Crediter(float montant)
    {
        solde += montant;
    }
    public Compte(Client client, float montant)
    {
        Proprietaire = client;
        solde = montant;
        code = ++compteur;
    }
    public Compte(Client client)
    {
        Proprietaire = client;
        solde = default;
        code = ++compteur;
    }
}
