﻿using System;
using clientBanque;
using compteBanque;
namespace compte;
static class ProgramCompte
{
    public static void MainCompte()
    {
        string  cin, nom, prenom, tel;
        float somme;

        Compte.ReinitialiserCompteur();
        // Création du compte 1
        Console.Clear();
        Console.WriteLine("Compte 1:");
        Console.Write("Donner Le CIN: ");
        cin = Console.ReadLine()??"";
        Console.Write("Donner Le Nom: ");
        nom = Console.ReadLine()??"";
        Console.Write("Donner Le Prénom: ");
        prenom = Console.ReadLine()??"";
        Console.Write("Donner Le numéro de téléphone: ");
        tel = Console.ReadLine()??"";

        Client client1 = new Client(cin, nom, prenom, tel);
        Compte compte1 = new Compte(client1);

        Console.WriteLine("Détails du compte:");
        Console.WriteLine("************************");
        compte1.Afficher();
        Console.WriteLine("************************");
                // Opération de crédit sur le compte 1
        bool a;
        while (true) {
            Console.Write("Donner le montant à déposer: ");
            a = float.TryParse(Console.ReadLine()??"", out somme);

            if (a) break;
        }

        compte1.Crediter((float)somme);
        Console.WriteLine("Opération bien effectuée");
        Console.WriteLine("************************");
        //Détails du compte après opération de crédit:
        compte1.Afficher();
        Console.WriteLine("************************");
                // Opération de débit sur le compte 1
        while (true){        
            Console.Write("Donner le montant à retirer: ");
            a = float.TryParse(Console.ReadLine()??" ", out somme);
            if(a) break;
        }
        compte1.Debiter((float)somme);
        Console.WriteLine("Opération bien effectuée");
        Console.WriteLine("************************");
        //Détails du compte après opération de débit:
        compte1.Afficher();
        Console.WriteLine("************************");
                // Création du compte 2
        Console.WriteLine("Compte 2:");
        Console.Write("Donner Le CIN: ");
        cin = Console.ReadLine()??"";
        Console.Write("Donner Le Nom: ");
        nom = Console.ReadLine()??"";
        Console.Write("Donner Le Prénom: ");
        prenom = Console.ReadLine()??"";
        Console.Write("Donner Le numéro de téléphone: ");
        tel = Console.ReadLine()??"";

        Client client2 = new Client(cin, nom, prenom, tel);
        Compte compte2 = new Compte(client2);

        Console.WriteLine("Détails du compte 2:");
        Console.WriteLine("************************");
        compte2.Afficher();
        Console.WriteLine("************************");
        Console.WriteLine("Crediter le compte 2 à partir du compte 1:");
        while (true){
            Console.Write("Donner le montant à déposer:");
            a = float.TryParse(Console.ReadLine() ?? "1000", out somme);
            if (a) break;
        }
        Console.WriteLine("Opération bien effectuée");
        compte2.Crediter((float)somme,compte1);
        Console.WriteLine("Débiter le compte 1 et créditer le compte 2");
        while (true){
            Console.Write("Donner le montant à retirer: ");
            a = float.TryParse(Console.ReadLine()??"", out somme);
            if (a) break;
        }
        compte1.Debiter((float)somme, compte2);
        Console.WriteLine("************************");
        Console.WriteLine("Détails des comptes après opération de débit sur le compte 1 et crédit sur le compte 2: \n");
        compte1.Afficher();
        Console.WriteLine("************************\n************************");
        compte2.Afficher();
        Console.WriteLine("************************");
        

        // Affichage du nombre total de comptes créés
        Console.WriteLine(Compte.NombreCompteCree());
       
    }
}

