using System;
namespace clientBanque;

class Client{
    // Attributs
    public string CIN { get; set; }
    public string Nom { get; set; }
    public string Prenom { get; set; }
    public string Tel { get; set; }

    // Constructeur
    public Client(string cin, string nom, string prenom, string tel){
        CIN = cin;
        Nom = nom;
        Prenom = prenom;
        Tel = tel;
    }
    // Constructeur pour initialiser CIN, nom et prénom
    public Client(string cin, string nom, string prenom){
        CIN = cin;
        Nom = nom;
        Prenom = prenom;
        Tel = "";
    }
        // Méthode pour afficher les informations du client
    public void Afficher(){
        Console.WriteLine($"CIN : {CIN}");
        Console.WriteLine($"Nom : {Nom}");
        Console.WriteLine($"Prénom : {Prenom}");
        Console.WriteLine($"Tél : {Tel}");
        }
}
