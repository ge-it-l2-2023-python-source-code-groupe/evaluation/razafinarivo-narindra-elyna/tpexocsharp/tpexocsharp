﻿using System;
using compte;
using cercle;
using jeu;
using Horloge;
namespace tpcsharp1;

class Program
{
    static void Main(string[] args)
    {
        string tp = "1";
        while(tp!= "5")
        {
            Console.Write("Menu principal : "+
                        "\n1 - TP basique n°1: Banque"+
                        "\n2 - TP basique n°2: Cercle"+
                        "\n3 - TP basique n°3: Jeu"+
                        "\n4 - TP basique n°4: Horloge"+
                        "\n5 - Quitter"+
                        "\n\n Quel est votre choix? ");
            tp = Console.ReadLine()??"";
            switch (tp)
            {
                case "1":
                    ProgramCompte.MainCompte();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case "2":
                    ProgramCercle.MainCercle();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case "3":
                    ProgramJeu.MainJeu();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case "4":
                    ProgramHorloge.MainHorloge();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                    
            }
        }
    }
}
